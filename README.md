# GitLab Activity Tests

This repository is used for testing [gitlab-activity](https://gitlab.com/mahendrapaipuri/gitlab-activity)
tool. The repo provides bunch of issues and MRs that will be used in testing of `gitlab-activity`. 

Both issues and MRs are created to cover several different cases by using different labels. All of them 
will be used as source of truth in testing.
